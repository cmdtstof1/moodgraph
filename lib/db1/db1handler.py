#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("here")

import sqlite3
import os
#TODO check with __init__.py > init1.py >>> not working!
from lib.db1 import tbl1
from lib.db1 import tbl2
from lib.db1 import tbl3
import lib.util.datehelper as dthnd


class Dbhandler:
    '''
    handler for one db. the next db would get an other handler.
    the handler is only for managing db (open, read, write, ...)
    the data should be handled in the tbl object
    
    '''

    def __init__(self, app, opt):
        self.app = app
        self.opt = opt
        self.cfg = app.cfg['db1']
        self.tbls = {    #TODO tbls list must be ordered, when FK is used ??? it works!
            'tbl1': [],  #tbl object: users
            'tbl2': [],  #moods
            'tbl3': []   #telegram messages (update_id)
        }
        #~ self.opendb()
        #~ self.setupdb()

    def updateV1toLatest(self):
        cupw("updateV1toLatest")
        
        self.opendb()
        stm = "ALTER TABLE tbl2 RENAME TO tbl2_old"
        self.cur.execute(stm)
        self.cur.execute(self.tbls['tbl2'].createscript['latest'])
        stm = "INSERT INTO tbl2 SELECT * FROM tbl2_old"
        self.cur.execute(stm)

        stm = "DROP TABLE tbl2_old"
        self.cur.execute(stm)
        
        self.commitdb()
        self.closedb()


    def opendb(self):
        '''
        opens db
        db file will be created if not exist but dir data must exist!!!!
        '''
        #TODO create dev db if --dev
        self.conn = sqlite3.connect(self.cfg['dbname'], check_same_thread=False)
        self.cur = self.conn.cursor()
        self.tbls['tbl1'] = tbl1.Tbl1(self)
        self.tbls['tbl2'] = tbl2.Tbl2(self)
        self.tbls['tbl3'] = tbl3.Tbl3(self)
        cupw("db open " + self.cfg["dbname"])       
    
    def setupdb(self):
        for tbl in self.tbls:
            self.tbls[tbl].setcolumns()
        cupw("db setup " + self.cfg["dbname"])

    def existTbl(self, tbl): # > li tblname / None
        stm = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + tbl + "';"
        l1 = self.cur.fetchone()
        return l1
    
    
    def createdb(self):
        '''
        del db file if exist
        creates db file with tables (!!!dir "data" must exist)
        commits and closes db
        '''
        self.app.www.delGraphDir() # wenn db gelöscht wird, kann auch gleich graphdir gelöscht
        if os.path.isfile(self.cfg['dbname']): 
            os.remove(self.cfg['dbname']) #delete file
            cupw("db file deleted=", self.cfg['dbname'])

        self.opendb()
        for tbl in self.tbls:
            cupw("tbl=", tbl)
            # ~ cupw(tbl + " drop if exist...")
            # ~ self.tbls[tbl].drop()
            cupw(tbl + " create...")
            self.tbls[tbl].create()
        self.commitdb()
        self.closedb()
        # ~ self.settblcol()

        
    def settblcol(self):
        '''
        creates colum list out of tables
        TODO: für was und wo genutzt??? 
        '''
        for tbl in self.tbls:
            self.tbls[tbl].setcolumns()
        
 
    def createtd(self):
        for tbl in self.tbls:
            cupw(tbl + " testdata...")
            self.tbls[tbl].createtd()
        self.commitdb()

    def commitdb(self):
        cupw("db commit...")
        self.conn.commit()

    def closedb(self):
        '''
        closes db
        '''
        cupw("db close " + self.cfg["dbname"])
        self.conn.close()

##################
#~ read, write, update, del
#~ alles als dict zurück (wenn möglich!!!)
#~ >>>>> memory2.py
        
    def getMoodMax(self, tgid):       #moodYmax = self.app.db1.getMoodMax(duser['tgid'])
        stm = "select max(mood) from tbl2 where tgid = " + str(tgid)
        self.cur.execute(stm)
        return self.cur.fetchone()
        
    def getMoodMin(self, tgid): #moodYmin = self.app.db1.getMoodMin(duser['tgid'])
        stm = "select min(mood) from tbl2 where tgid = " + str(tgid)
        self.cur.execute(stm)
        return self.cur.fetchone()
    
    def getMinTmstmp(self, tgid): #ac['ymin'] = self.app.db1.getMinTmstmp(ac['tgid'])
        stm = "select min(tmstmp) from tbl2 where tgid = " + str(tgid)
        self.cur.execute(stm)
        return self.cur.fetchone()

    def getMaxTmstmp(self, tgid):
        stm = "select max(tmstmp) from tbl2 where tgid = " + str(tgid)
        self.cur.execute(stm)
        return self.cur.fetchone()
        
    def getMinMood(self, tgid):
        stm = "select min(mood) from tbl2 where tgid = " + str(tgid)
        self.cur.execute(stm)
        return self.cur.fetchone()
    
    def getMaxMood(self, tgid):
        stm = "select max(mood) from tbl2 where tgid = " + str(tgid)
        self.cur.execute(stm)
        return self.cur.fetchone()


    
    def checkUpdateIdExist(self, d1): #<d1 >0/1
        stm = "select * from tbl3 where update_id=" + str(d1['update_id'])
        self.cur.execute(stm)
        l1 = self.cur.fetchone()
        if l1 is None:
            self.addUpdateId(d1)
            return 0
        else:
            return 1
        
    def addUpdateId(self, d1):
        stm = 'insert into tbl3 (update_id) values (' + str(d1['update_id']) + ')'
        cupw(stm)
        self.cur.execute(stm)
        #~ cupw("added msg update_id " + str(d1['update_id']))
         
    
    def userHasData(self, tgid): #> count
        stm = 'select count(mood) from tbl2 where tgid =' + str(tgid)
        self.cur.execute(stm)
        return self.cur.fetchone()[0]
    
    def existsUserId(self, tgid): # existsUserId(self, tgid) >d1
        #~ check if user exist. return -1 / row as dict
        stm = "select * from tbl1 where tgid=" + str(tgid)
        self.cur.execute(stm)
        l1 = self.cur.fetchone()
        #~ cupw(l1) #None
        #~ cupw(type(l1))
        d1 = {}
        if l1 is not None:
            d1 = dict(zip(self.tbls['tbl1'].col, l1))
        return d1

    def getLastMood(self, tgid): # <tgid >d
        #~ select *, max(tmstmp) from tbl2 where tgid = 584395023
        stm = 'select *, max(tmstmp) from tbl2 where tgid = ' + str(tgid)
        self.cur.execute(stm)
        l1 = self.cur.fetchone()
        d1 = {}
        if l1 is not None:
            d1 = dict(zip(self.tbls['tbl2'].col, l1))
        return d1


    def delLastMood(self, tgid): #d3 = app.db1.delLastMood(d2['tgid']) > d1 = self.getLastMood(tgid)
        d1 = self.getLastMood(tgid)
        #~ cupw(d1)
        if d1['idx'] is not None:
            self.delMood(d1['idx'])
            d1 = self.getLastMood(tgid)
        return d1
            
    
    def delUser(self, d1):
        stm = "delete from tbl2 where tgid = " + str(d1['tgid']) #TODO warum nicht cascaded????
        self.cur.execute(stm)
        stm = "delete from tbl1 where tgid = " + str(d1['tgid'])
        self.cur.execute(stm)
        self.commitdb()
    
    def delMood(self, idx):
        stm = "delete from tbl2 where idx = " + str(idx)
        #~ cupw(stm)
        self.cur.execute(stm)
    
    def addMood(self, d1):         #~ add mood. <row as dict. >l1 lastid 
        #~ cupw(d1)
        stm = "select * from tbl2 where tgid=" + str(d1['tgid']) + " and tmstmp=" + str(d1['tmstmp'])
        #~ cupw(stm)
        self.cur.execute(stm)
        l1 = self.cur.fetchone()
        #~ cupw(l1)
        #~ cupw(type(l1))
        if l1 is None:
            stm = "insert into tbl2 (tgid, tmstmp, mood, comment) values (" + str(d1["tgid"]) + ","+ str(d1["tmstmp"]) +","+d1["mood"]+",'"+d1['comment']+"')"
            cupw(stm)
            self.cur.execute(stm)
            self.commitdb()
            return self.gett2LastId()            
        else:
            cupw("mood already exist")
            return l1

    def addUser(self,d1): #add user <d1 > # existsUserId(self, tgid) >d1
        d1['lstmod'] = dthnd.intTmstmpNow() #> int(timestamp now)
        #~ d1['www'] = self.wwwCreateStr()
        d1['www'] = self.app.www.createNewWwwStr()
        stm = 'insert into tbl1 (tgid, tguser, lstmod, www) values (' \
                + str(d1['tgid']) + ',"'+ d1['tguser'] +'",'+ str(d1['lstmod'])+',"'+str(d1['www'])+   '")'
        cupw(stm)
        self.cur.execute(stm)
        self.commitdb()
        return self.existsUserId(d1['tgid'])
    
    def wwwExistInTbl(self, str1):
        stm = "select * from tbl1 where www = '" + str1 + "';"
        #~ cupw(stm)
        self.cur.execute(stm)
        if self.cur.fetchone() is not None:
            return 1
        else:
            return 0
        
    def gett1tupdict(self, user): # duser = self.app.db1.gett1tupdict(user)
        return dict(zip(self.tbls['tbl1'].col, user))
    
    def gett2LastId(self):
        stm = 'select seq from sqlite_sequence where name = "tbl2"'
        self.cur.execute(stm)
        return self.cur.fetchone()
        
    def getUsers(self): #cur1 = app.db1.getUsers()
        stm = 'select * from tbl1'
        #~ for user in self.cur.execute(stm): #corrct!!!!
            #~ cupw(user)
        return self.cur.execute(stm)

    def getMoods(self, tgid): #cur2 = self.app.db1.getMoods(duser['tgid'], from, till)
        stm = 'select * from tbl2 where tgid = ' + str(tgid) + " order by tmstmp desc"
        return self.cur.execute(stm)
        
