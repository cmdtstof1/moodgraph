#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
# cupw("here")


class Tbl1:

    def __init__(self, db):
        self.db = db
 
#~ (tgid, tguser, lstmod, www)
            
        self.createscript = """CREATE TABLE tbl1
            (tgid INTEGER PRIMARY KEY,
            tguser TEXT,
            lstmod INTEGER,
            www TEXT
            );"""
        self.col = []

    def drop(self):
        try:
            self.db.cur.execute("DROP TABLE tbl1")
        except:
            cupw("tbl1 not exist")

    def create(self):
        self.db.cur.execute(self.createscript)
        
    def setcolumns(self):
        self.col = self.getcolumns()
    
    def getcolumns(self):
        self.db.cur.execute('SELECT * FROM tbl1')
        return list(map(lambda x: x[0], self.db.cur.description)) #gets column list
 

    def createtd(self):
        pass
