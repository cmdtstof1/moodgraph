#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
# cupw("here")

#~ import datetime as dt #TODO use util.datehandler!!!!
import lib.util.datehelper as dthnd
import math
from random import randint

class Tbl2:

    def __init__(self, db):
        self.db = db # <lib.db1.db1handler.Dbhandler object at 0x7fcff10e26d8>
        #~ cupw(dir(self.db))
        #~ cupw(self.db.cfg['t2tdcount'])
        self.createscript = {
            'latest': '''
CREATE TABLE tbl2
(idx INTEGER PRIMARY KEY AUTOINCREMENT,
tgid INTEGER,
tmstmp INTEGER,
mood INTEGER,
comment TEXT,
UNIQUE(tgid, tmstmp),
FOREIGN KEY(tgid) REFERENCES tbl1(tgid) ON UPDATE CASCADE
);
                ''',
            'v1': '''
CREATE TABLE tbl2
(idx INTEGER PRIMARY KEY AUTOINCREMENT,
tgid INTEGER,
tmstmp INTEGER,
mood INTEGER,
comment TEXT,
UNIQUE(tgid, tmstmp),
FOREIGN KEY(tgid) REFERENCES tbl1(tgid) ON UPDATE CASCADE
);
                '''
        }
        self.col = []

    def drop(self):
        try:
            self.db.cur.execute("DROP TABLE tbl2")
        except:
            cupw("tbl2 not exist")

    def create(self):
        self.db.cur.execute(self.createscript['latest'])
        
    def setcolumns(self):
        self.col = self.getcolumns()
    
    def getcolumns(self):
        self.db.cur.execute('SELECT * FROM tbl2')
        return list(map(lambda x: x[0], self.db.cur.description)) #gets column list
 

    def createtd1(self):
        data1 = [
            (1, 584395023, 1588281638, "4", "comment1"),
            (2, 584395023, 1588291638, "5", "comment2"),
            (3, 584395023, 1588301638, "6", "comment3"),
            (4, 584395023, 1588311638, "2", "comment4"),
            (5, 584395023, 1588321638, "-2", "comment5"),
            (6, 584395023, 1588331638, "0", "comment6"),
        ]
        self.db.cur.executemany('INSERT INTO tbl2 VALUES (?,?,?,?)', data1)

    def createtd(self):
        tgid = 584395023
        tmstmp = dthnd.intTmstmpNow()
        data1 = []
        sinstart = 0
        sinstep = 0.1
        sinyexp = self.db.cfg['t2tmax'] # 10 = 0-20 amplitude = width)
        sini = sinstart

        for i in range(0, self.db.cfg['t2tdcount']):
            s = math.sin(sini)
            y = s * sinyexp
            yint = int(y+0.5)
            mood = yint + sinyexp -1
            comment = "comment" + str(i)
            tup1 = (i, tgid, tmstmp, mood, comment )
            data1.append(tup1)
            sini += sinstep
            tmstmp -= 60*60*randint(12, 72) + 60*randint(1,60)
            
        #~ cupw(data1)
        self.db.cur.executemany('INSERT INTO tbl2 VALUES (?,?,?,?,?)', data1)
        
            
        


