#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
# cupw("here")


class Tbl3:

    def __init__(self, db):
        self.db = db
        self.createscript = """CREATE TABLE tbl3
            (update_id INTEGER PRIMARY KEY
            );"""
        self.col = []

    def drop(self):
        try:
            self.db.cur.execute("DROP TABLE tbl3")
        except:
            cupw("tbl3 not exist")

    def create(self):
        self.db.cur.execute(self.createscript)
        
    def setcolumns(self):
        self.col = self.getcolumns()
    
    def getcolumns(self):
        self.db.cur.execute('SELECT * FROM tbl3')
        return list(map(lambda x: x[0], self.db.cur.description)) #gets column list

    def createtd(self):
        pass

