#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("here")

    #~ "mood": {
        #~ "cmtlen": 15,
        #~ "cmtdelimiter": " - "

class Mood:
    
    def __init__(self, app, opt):
        self.app = app
        self.opt = opt
        self.cfg = app.cfg['mood']

                    
    def parseMood(self, str1): #<str >1/0, mood, comment
        #~ cupw(str1)
        l1 = str1.split(" ", 1)
        #~ cupw(l1)
        mood = ""
        comment = ""
        try: 
            int(l1[0])
            mood = l1[0]
            #~ cupw(len(l1))
            if len(l1) > 1:
                comment = l1[1][:self.cfg['cmtlen']-1] 
                #TODO check about sql injections!!!!!
            ret = 1
        except ValueError:
            ret = 0
        return (ret, mood, comment)
    
    def moodConcat(self, strold, strnew): #drow['comment'] = self.app.mood.moodConcat(ac['d'][datum]['comment'], drow['comment'])
    #~ def moodConcat(self, strold, strnew, moodold, moodnew): #drow['comment'] = self.app.mood.moodConcat(ac['d'][datum]['comment'], drow['comment'])
#drow['comment'] = self.app.mood.moodConcat(ac['d'][datum]['comment'], drow['comment'], ac['d'][datum]['mood'], drow['comment'])

        '''
        if more then 1 mood / day
        concat comments. latest first = hier last
        '''
        if strold is None:
            strold = self.cfg['cmtmultidelmiter']
        
        if strnew is None:
            strnew = self.cfg['cmtmultidelmiter']
        
        str1 = strold + self.cfg['cmtmultidelmiter'] + strnew[:self.cfg['cmtlen']-1]
        #~ str1 = strold + "("+str(moodold)+")"+self.cfg['cmtmultidelmiter'] + strnew[:self.cfg['cmtlen']-1]+"("+str(moodnew)+")"
        return str1
    
