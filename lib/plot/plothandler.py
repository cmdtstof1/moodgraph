#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("here")

import lib.util.datehelper as dthnd
from lib.plot import plotvertikal 

class Plothandler:

    def __init__(self, app, opt):
        self.app = app
        #~ cupw(dir(app))
        self.cfg = app.cfg['plot']
    
    def createplAll(self):
        cupw("createpl")
        users = []
        stm = 'select * from tbl1'
        for user in self.app.db1.cur.execute(stm): #TODO >>>why-false working ???
            users.append(user)
        for user in users:    
            #~ cupw(user)
            duser = self.app.db1.gett1tupdict(user)
            self.createplUser(duser)
       
    def createplUser(self, duser): 
        if self.app.db1.userHasData(duser['tgid']) > 0: #>count
            data1 = self.interpolX(duser['tgid']) #>array of dict
            #~ for dat in dict1:
                #~ cupw(dat)
            pl = plotvertikal.PlotVertikal(self.app)
            pl.plotCreate(duser, data1)

        else:
            self.app.www.createEmptySite(duser['www']) #<t1.www
        
        
        

    def testdraw(self, tgid):
        fbg = self.cfg['plt']['fbg']
        ffg = self.cfg['plt']['ffg']
        yexp = 5
        cur2 = self.app.db1.getMoods(tgid)
        for tmoodi in cur2:
            #~ cupw(mood)
            yp = int(tmoodi[3])
            #~ cupw(yp)
            tmstmp = str(tmoodi[2])
            dat = str(dthnd.datetime.fromtimestamp(tmoodi[2]).date())
            str1 = fbg*yp + ffg + fbg*(yexp*2 - yp - 1) + "  " + tmstmp + " " + dat 
            #~ cupw(str1)
            print(str1)



        
    def interpolX(self, tgid):    
        ac = {}
        ac['tgid'] = tgid
        ac['tsmin'] = self.app.db1.getMinTmstmp(ac['tgid']) #(1579991662,)
        if ac['tsmin'] is not None: #da sind daten vorhanden
            ac['tsmin'] = ac['tsmin'][0]
            ac['tsmax'] = self.app.db1.getMaxTmstmp(ac['tgid'])[0] #'tmax': 1588545262
            ac['tsmaxdt'] = dthnd.tmstmp2datestr(ac['tsmax'])
            ac['tsmindt'] = dthnd.tmstmp2datestr(ac['tsmin'])
            
            ac['d'] = {}
            for row1 in self.app.db1.getMoods(ac['tgid']):
                #~ cupw(row1)
                #TODO row1 in dict mit columns!
                drow = {}
                drow['mood'] = row1[3]
                drow['comment'] = row1[4]
                datum = dthnd.tmstmp2datestr(row1[2])
                
                if datum in ac['d']: #resample
                    #TODO resample stimmt nicht!!???
                    #~ cupw("datum=", datum , "old=", ac['d'][datum]['mood'], "new=", drow['mood'] )
                    drow['mood'] = int((ac['d'][datum]['mood'] + drow['mood']) / 2 + 0.5)
                    #~ cupw("newnew=", drow['mood'])
                    
                    drow['comment'] = self.app.mood.moodConcat(ac['d'][datum]['comment'], drow['comment'])
                    #~ drow['comment'] = self.app.mood.moodConcat(ac['d'][datum]['comment'], drow['comment'], ac['d'][datum]['mood'], drow['mood'])
                    
                ac['d'][datum] = drow
            
            #~ i = 1 #TODO check if select distinct gleichviel gibt
            #~ for key in sorted(ac['d'], reverse=True):
                #~ print(i, key, ac['d'][key])
                #~ i += 1    

            #~ ####################check if wholes > interpolieren!!!!
            #bestehendes set
            sac = set()
            for dat in ac['d']:
                sac.add(dat)
            
            #testset
            stest = set()
            tist = ac['tsmindt']
            while tist <= ac['tsmaxdt']:
                stest.add(tist) 
                tist = dthnd.dateAddDay(tist) #yyyymmdd + 1d
                
            #add diff set
            sdiff = (stest - sac)
            #~ cupw(sdiff)
            for dat in sdiff:
                ac['d'][dat] = {'mood': self.cfg['data']['vfehlt']}
            
            del sac
            del stest
            del sdiff 

            #and now further with a list. #von anfang als list??? nein, insert ist einfacher!!!!!!!!!!!!
            ac['dl'] = []
            for key in sorted(ac['d'], reverse=True): 
                #~ cupw(key)
                dtemp = {}
                dtemp['datum'] = key
                for (k,v) in ac['d'][key].items():
                    dtemp[k] = v
                ac['dl'].append(dtemp)
                
            #~ cupw(ac['dl'])
            #~ del ac['d']

#~ [1] https://www.wikihow.com/Interpolate
#~ y = y1 + ((x - x1)/(x2 - x1) * (y2 - y1)) 
#~ x=neues datum, #~ y = neuer mood
#~ x1, x2, y1, y2 = vorhergehendes (-1) / nachfolgendes (+1) datum / mood
            
            for i in range(0, len(ac['dl'])):
                #~ cupw(ac['dl'][i])
                if ac['dl'][i]['mood'] is self.cfg['data']['vfehlt']:
                    #~ cupw("fehlt")
                    x = dthnd.dateStr2tmstmp(ac['dl'][i]['datum'])
                    #go up
                    x1 = dthnd.dateStr2tmstmp(ac['dl'][i-1]['datum'])
                    y1 = ac['dl'][i-1]['mood']
                    #go down #TODO check end of list! (but last should be exist!)
                    j = i + 1
                    while True:
                        if ac['dl'][j]['mood'] is not self.cfg['data']['vfehlt']:
                            x2 = dthnd.dateStr2tmstmp(ac['dl'][j]['datum'])
                            y2 = ac['dl'][j]['mood']
                            break
                        else:
                            j += 1

                    y = y1 + ((x - x1)/(x2 - x1) * (y2 - y1))
                    y = int(y + 0.5)    
                    ac['dl'][i]['mood'] = y
                    ac['dl'][i]['comment'] = self.cfg['data']['vfehlt']    
            
            return ac['dl']    
                    

            
            
            
            
            

    
########################################
if __name__ == "__main__":
# all the vars must be set hier, that dbhandler can run independend!!!
    cupw("executet as main")

    #~ app = DummyApp()
    #~ self.app.log.write("plotter ready...")
