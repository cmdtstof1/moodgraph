#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
import lib.util.datehelper as dthnd

#~ cupw("here")

        #~ "plt": {
            #~ "canwidth": 80,
            #~ "pltwidth": 40

        # canvas = can
        # canvas [0,0] = links oben
        # canvas array = y, x
        # canvas x = horizontal, = cft['plt']['canwidth']
        # canvas y = len(data)
        
        #~ ac['moodMax'] = self.app.db1.getMoodMax(tgid)[0]
        



class PlotVertikal:

    def __init__(self, app):
        self.app = app
        self.cfg = app.cfg['plot']
        #~ self.lcan = [] # canvas array = y, x
        self.weekday = ["s","m","t","w","t","f","s"]
        
    def plotCreate(self, duser, data1):
        self.duser = duser
        self.plt = self.cfg['plt']
        
        self.ac = {}
        self.ac['moodYmax'] = self.app.db1.getMoodMax(duser['tgid'])[0]
        self.ac['moodYmin'] = self.app.db1.getMoodMin(duser['tgid'])[0]
        
        self.ac['moodSpan'] = self.ac['moodYmax'] - self.ac['moodYmin']
        
        self.ac['moodShift'] = 0
        if self.ac['moodYmin'] < 0:
            self.ac['moodShift'] = abs(self.ac['moodYmin'])
        if self.ac['moodYmin'] > 0:
            self.ac['moodShift'] = 0 - self.ac['moodYmin']

        if self.ac['moodSpan'] >= self.cfg['plt']['pltwidth']:
            self.ac['yexp'] = self.cfg['plt']['pltwidth'] / self.ac['moodSpan']
        else:
            self.ac['yexp'] = 1
        
        self.ac['lc'] = self.app.cfg['www']['csscolor'] #"csscolor": ["c1","c2","c3","c4","c5","c6"]
        self.ac['lclen'] = len(self.ac['lc'])

        #~ cupw(self.ac)
        graphStr = "<div class='ascii-art fett'>\n"
        for dat in data1:
            #~ cupw(dat)
            str1 = self.moodLine(dat)
            #~ print(str1)
            graphStr += str1 + "\n"
        
        graphStr += "</div>\n"
        #~ cupw(graphStr)
        self.app.www.htmlMaker(duser, graphStr)


    def moodLine(self, dat):
        
        yp = int((int(dat['mood']) + self.ac['moodShift']) * self.ac['yexp'] )
        #~ cupw(yp)
        #~ str1 = fbg*yp + ffg + fbg*(yexp*2 - yp - 1) + "  " + datum + dat[4] 
        #~ str1 = self.cfg['plt']['fbg']*yp + self.cfg['plt']['ffg'] + self.cfg['plt']['fbg']*(self.ac['yexp']*2 - yp - 1) + "  " + dat['datum'] + " " + dat['comment'] 

        proz = (dat['mood'] + self.ac['moodShift'] + 1) / (self.ac['moodYmax'] + self.ac['moodShift'] + 1)
        col = int(proz * (self.ac['lclen'] -1) + 0.5)
        #~ cupw("mood=",dat['mood'], "proz=", proz, "col=", col)
        
        str1 = '<p class="' + self.ac['lc'][col] + '">' 
        str1 += self.cfg['plt']['fbg']*yp 
        str1 += self.cfg['plt']['ffg'] 

        #~ str1 += " (" + str(dat['mood']) + ") " + dat['datum']
        weekdaystr = self.weekday[dthnd.weekdayFromYyyymmdd(dat['datum'])] + " "
        str1 += " (" + str(dat['mood']) + ") " + weekdaystr + dat['datum']

        str1 += " " + dat['comment'][:self.cfg['plt']['cmtmaxlen']-1]
        str1 += '</p>'
        #~ cupw(str1)
        return str1        
        


