#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("here")

from telegram.ext import Updater, CommandHandler
from telegram.ext import MessageHandler, Filters
import logging

#used only for restart
import os
import sys
from threading import Thread

class Tgbot:
    '''
the bot! only updater and dispatcher (commands) handling here.
the logic is in worker
    '''
    
    def __init__(self, app, opt):
        self.app = app
        #~ cupw(dir(app))
        self.opt = opt
        self.tgToken = self.app.cfg['telegram']['token']
        self.tgAdmin = self.app.cfg['telegram']['admin']

    def update2dict(self, update):
        dict1 = {}
        dict1['tgid'] = update.effective_chat.id
        dict1['text'] = update.effective_message.text
        dict1['date'] = update.effective_message.date
        dict1['tguser'] = update.effective_user.username
        return dict1


    def run(self):
        
        updater = Updater(self.tgToken, use_context=True)
        dispatcher = updater.dispatcher
        
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                             level=logging.INFO)

        
        def start(update, context):
            txt1 = self.app.worker.tgstarthnd() #> str starttxt
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt1)
            
        def inviteme(update, context):
            txt1 = self.app.worker.tginvitemehnd(self.update2dict(update)) #<tgid, username >str text
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt1)

        def echo(update, context):
            txt1 = self.app.worker.tgmoodhnd(self.update2dict(update)) #<tgid, mood, dateDt >str text
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt1)
 
        def dellast(update, context):
            txt1 = self.app.worker.tgdellasthnd(self.update2dict(update))
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt1)
        
        def delme(update, context):
            txt1 = self.app.worker.tgdelmehnd(self.update2dict(update))
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt1)
        
        def delmedelme(update, context):
            txt1 = self.app.worker.tgdelmedelmehnd(self.update2dict(update))
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt1)
        
        def help(update, context):
            txt1 = self.app.worker.tghelphnd(self.update2dict(update))
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt1)
    
        def feedback(update, context):
            dict1 = self.update2dict(update)
            txt1 = self.app.worker.tgfeedbackhnd(dict1)
            context.bot.send_message(chat_id=dict1['tgid'], text=txt1)
            txt2 = "mgbot feedback from @" + dict1['tguser'] + ": " + dict1['text']
            context.bot.send_message(chat_id=self.tgAdmin, text=txt2)


 
        ################################vielleicht wieder mal raus?
        def stop_and_restart():
            """Gracefully stop the Updater and replace the current process with a new one"""
            updater.stop()
            os.execl(sys.executable, sys.executable, *sys.argv)
        
        def restart(update, context):
            update.message.reply_text('Bot is restarting...')
            Thread(target=stop_and_restart).start()
        
        #~ def stopbot():
            #~ """Gracefully stop the Updater and replace the current process with a new one"""
            #~ updater.stop()
            #~ os.execl(sys.executable, sys.executable, *sys.argv)
        
        #~ def stop(update, context):
            #~ update.message.reply_text('Bot is stoping...')
            #~ cupw(dir(Thread))
            #~ Thread(target=stopbot)._stop() #stop soll es nicht geben. ??? 
        
        #~ https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets#simple-way-of-restarting-the-bot
        dispatcher.add_handler(CommandHandler('restartrestart', restart, filters=Filters.user(user_id=self.tgAdmin)))
        #~ dispatcher.add_handler(CommandHandler('stopstop', stop, filters=Filters.user(user_id=app.tgAdmin)))
        ##################################33
 
    

        start_handler = CommandHandler('start', start)
        dispatcher.add_handler(start_handler)

        inviteme_handler = CommandHandler('inviteme', inviteme)
        dispatcher.add_handler(inviteme_handler)

        echo_handler = MessageHandler(Filters.text & (~Filters.command), echo)
        dispatcher.add_handler(echo_handler)
        
        dellast_handler = CommandHandler('dellast', dellast)
        dispatcher.add_handler(dellast_handler)
        
        delme_handler = CommandHandler('delme', delme)
        dispatcher.add_handler(delme_handler)
        
        delmedelme_handler = CommandHandler('delmedelme', delmedelme)
        dispatcher.add_handler(delmedelme_handler)
        
        help_handler = CommandHandler('help', help)
        dispatcher.add_handler(help_handler)

        feedback_handler = CommandHandler('feedback', feedback)
        dispatcher.add_handler(feedback_handler)

        cupw("bis hier geschafft. jetzt warten ...")
        updater.start_polling()
        updater.idle()
        
