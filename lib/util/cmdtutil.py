#!/usr/bin/env python3

from inspect import getframeinfo, stack
import ntpath

def prnwo(*arguments):
    message1 = ""
    caller = getframeinfo(stack()[1][0])
    filename = ntpath.basename(caller.filename)
    for e in arguments:
        message1 += " " + str(e)
    print("%s:%d - %s" % (filename, caller.lineno, message1))
