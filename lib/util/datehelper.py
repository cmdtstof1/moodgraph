#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("here")

# to use as: import lib.util.datehelper as dthnd

import datetime as dt


def weekdayFromYyyymmdd(str1): #<yyyymmdd >int (sun=0)
    format_str = '%Y%m%d'
    dt1 = dt.datetime.strptime(str1, format_str)
    return int(dt1.strftime("%w"))

def intTmstmpNow(): #> int(timestamp now)
    return int(dt.datetime.now().timestamp())
    
def dateDtnow():
    return dt.datetime.now()
    
def intTmstmpFromDt(dt1): #datedt >int(tmstmp)
    return int(dt.datetime.timestamp(dt1)) 
    
def dateSubDay(date_str): ##<yyyymmdd >yyyymmdd - 1d
    format_str = '%Y%m%d'
    dt1 = dt.datetime.strptime(date_str, format_str)
    dt2 = dt1 - dt.timedelta(days=1)
    return dt2.date().strftime("%Y%m%d")

def dateAddDay(date_str): #<yyyymmdd >yyyymmdd + 1d
    format_str = '%Y%m%d'
    dt1 = dt.datetime.strptime(date_str, format_str)
    dt2 = dt1 + dt.timedelta(days=1)
    return dt2.date().strftime("%Y%m%d")

    
def dateStr2tmstmp(date_str): #<yyyymmdd >tmstmp
    format_str = '%Y%m%d'
    dt1 = dt.datetime.strptime(date_str, format_str)
    return dt.datetime.timestamp(dt1)
    
def tmstmp2datestr(tmstmp): #<tmstmp >yyyymmdd
    return dt.datetime.fromtimestamp(tmstmp).date().strftime("%Y%m%d")
