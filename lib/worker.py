#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("#################################here")

import lib.util.datehelper as dthnd
# ~ from lib.test import tester as tester

class Worker:
    
    def __init__(self, app):
    # ~ def __init__(self, app, opt):
        self.app = app #e.g. self.app['cfg']
        self.opt = app.opt


##############optionParser
   
    def main(self):
        cupw(self.opt)
        
        if self.opt['--test']:
            cupw("TODO --test")
            
            #test/ptb-test.py
            
            
            
            

        if self.opt['--db1create']:
            self.app.db1.createdb()
        
        # ~ if self.opt['--db1open']:
            # ~ self.app.db1.opendb()
            
        # ~ if self.opt['--db1setup']:
            # ~ self.app.db1.setupdb()

        
        if '--runtestfull' in self.opt and self.opt['--runtestfull']:
            self.setOptNull()
            tester.runtestfull(self.app)
        if '--runtest' in self.opt and self.opt['--runtest']:
            self.setOptNull()
            tester.runtest(self.app)

        #TODO dbupdate must be done better
        if '--dbupdate' in self.opt and self.opt['--dbupdate']:
            self.dodbupdate()
     
        if '--db1testdata' in self.opt and self.opt['--db1testdata']:
            #~ cupw("db1testdata")
            self.dodb1testdata()
            


        if self.opt['--tgbotrun']:
            cupw("tgbotrun...")
            self.app.db1.opendb()
            self.app.db1.setupdb()
            self.app.tgbot.run() #es geht erst wieder weiter, wenn bot killed!!!
        
    
        cupw("cleaner...")
        try:
            self.app.db1.commitdb()
            self.app.db1.closedb()
        except:
            pass

        
        
################################3
    
    
    
    
    def tgstarthnd(self): #> str starttxt
        cupw("/start")
        return self.app.dtxt['start']

    #~ dict1['tgid'], dict1['text'], dict1['date'], dict1['tguser']
    
 
    def tginvitemehnd(self, updict): #<tgid, username >str text
        cupw("/inviteme")
        d1 = {}
        d1['tgid'] = updict['tgid']
        d2 = self.app.db1.existsUserId(d1['tgid']) # existsUserId(self, tgid) >d1
        #~ cupw(d2)
        if not bool(d2):    
            d1['tguser'] = updict['tguser']
            #TODO get www from object ???
            #~ d1['www'] = www.createNewWww(self, d1['tgid'])
            d2 = self.app.db1.addUser(d1) #add user <d1 > # existsUserId(self, tgid) >d1
            #~ cupw(d2)
            txt1 = self.app.dtxt['inviteme']['ok']
        else:
            txt1 = self.app.dtxt['inviteme']['already']
        return txt1

    def tgmoodhnd(self, updict): #<tgid, mood, dateDt >str text
        #~ cupw("mood")
        d1 = {}
        (ret, d1['mood'], d1['comment']) = self.app.mood.parseMood(updict['text'])
        if ret:
            d1['tgid'] = updict['tgid']
            d2 = self.app.db1.existsUserId(d1['tgid']) # existsUserId(self, tgid) >d1
            if bool(d2):    
                d1['tmstmp'] = dthnd.intTmstmpFromDt(updict['date'])
                #~ cupw(d1)
                l1 = self.app.db1.addMood(d1)
                if l1 is not None:
                    self.app.plot.createplUser(d2) #sollte in eigenem thread laufen??
                    txt1 = self.app.dtxt['mood']['added'] + self.app.cfg['www']['graphurl'] + d2['www']
                else:
                    txt1 = self.app.dtxt['mood']['error']
            else:
                #~ cupw("user not exist")
                txt1 = self.app.dtxt['user']['notexist']
        else:
            txt1 = updict['text'] + self.app.dtxt['mood']['wrong']
        return txt1

    def tgdellasthnd(self, updict): 
        cupw("/dellast")
        d1 = {}
        d1['tgid'] = updict['tgid']
        d2 = self.app.db1.existsUserId(d1['tgid'])
        if bool(d2):
            d3 = self.app.db1.delLastMood(d1['tgid'])
            self.app.plot.createplUser(d2)
            txt1 = self.app.dtxt['dellast']['ok'] + str(d3['mood'])
        else:
            txt1 = self.app.dtxt['user']['notexist']
        return txt1

    def tgdelmehnd(self, updict):
        cupw("/delme")
        d1 = {}
        d1['tgid'] = updict['tgid']
        d2 = self.app.db1.existsUserId(d1['tgid'])
        if bool(d2):
            txt1 = self.app.dtxt['delme']['sure']
        else:
            txt1 = self.app.dtxt['user']['notexist']
        return txt1

    def tgdelmedelmehnd(self, updict):
        cupw("/delmedelme")
        d1 = {}
        d1['tgid'] = updict['tgid']
        d2 = self.app.db1.existsUserId(d1['tgid'])
        if bool(d2):
            d3 = self.app.db1.delUser(d1)
            self.app.www.delUserGraphDir(d2['www'])
            txt1 = self.app.dtxt['delme']['deleted']
        else:
            txt1 = self.app.dtxt['user']['notexist']
        return txt1
 
    def tghelphnd(self, updict):
        cupw("/help")
        d1 = {}
        d1['tgid'] = updict['tgid']
        d2 = self.app.db1.existsUserId(d1['tgid'])
        if bool(d2):
            txt1 = self.app.dtxt['help']['reg'] + self.app.cfg['www']['graphurl'] + d2['www']
        else:
            txt1 = self.app.dtxt['help']['notreg']
        return txt1

    def tgfeedbackhnd(self, updict):
        cupw("/feedback")
        return self.app.dtxt['feedback']['senderrply']


    def dodbupdate(self):
        self.app.db1.updateV1toLatest()
        
            
    
    def dodb1testdata(self):
        self.app.db1.createtd()
 
