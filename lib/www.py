#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("here")

import random
import string
from shutil import rmtree, copyfile
from pathlib import Path

class Www:

    def __init__(self, app, opt):
        self.app = app
        self.opt = opt
        self.cfg = app.cfg['www']


    def delGraphDir(self):
        cupw("del www graph dir")
        try:
            rmtree(self.cfg['wwwdir'])
        except:
            pass

    def delUserGraphDir(self, www):
        cupw("del user graph dir " + www)
        try:
            rmtree(self.cfg['wwwdir'] + www)
        except:
            pass
    
    
    
    def createNewWwwStr(self): #d1['www'] = www.createNewWwwStr(d1['tgid'])
        lettersAndDigits = string.ascii_letters + string.digits
        str1 = ''.join((random.choice(lettersAndDigits) for i in range(self.cfg['wwwfilelen'])))
        if self.app.db1.wwwExistInTbl(str1):
            self.createNewWwwStr()
        else:
            self.wwwMkdir(str1)
            return str1
    
    def dirExist(self, str1):
        dir1 = self.cfg['wwwdir'] + str1
        p = Path(dir1)
        if not p.exists():
            self.wwwMkdir(str1)
            
            
    def wwwMkdir(self, str1):
        dir1 = self.cfg['wwwdir'] + str1
        cupw("create www graph dir " + dir1)
        Path(dir1).mkdir(parents=True, exist_ok=True)
        self.createEmptySite(str1)
        
        copyfile(self.cfg['root']+'/'+self.cfg["csstmpl"], dir1 + '/'+self.cfg["csstmpl"])



    def createEmptySite(self, str1): #<t1.www
        dir1 = self.cfg['wwwdir'] + str1
        filename = dir1 + "/index.html"
        str1 = '''
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>yur mood-graph</title>
</head>
<body>

<h1>this will become yur mood-graph</h1>
<p>but until know there is no data left...</p>

<h1>have a good mood</h1>

</body>
</html>    
        '''
        with open(filename, "w") as text_file:
            text_file.write(str1)
        
        cupw("empty html created " + filename)
    

    def htmlMaker(self, duser, graphStr): #self.app.www.htmlMake(duser, graphStr)
        #~ cupw(duser) #{'www': '012K456b89', 'tguser': 'stof999', 'lstmod': 1588331613, 'tgid': 584395023}
        self.dirExist(duser['www'])
        filename = self.cfg['wwwdir'] + duser['www'] + "/index.html"
        htmlStr = '''
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="
'''
        htmlStr += self.cfg['csstmpl']
        htmlStr += '''
" type="text/css" >
<title>yur mood-graph</title>
</head>
<body>
'''
        htmlStr += '<h1>' + duser['tguser'] + ' mood-graph</h1>\n'
        htmlStr += '<div class="ascii-art">'
        htmlStr += graphStr
        htmlStr += '</div>'
        htmlStr += '''
<h1>have a good mood</h1>

</body>
</html>    
        
        '''
        
        #~ cupw(htmlStr)
        with open(filename, "w") as text_file:
            text_file.write(htmlStr)
        
        cupw("html created " + filename)
