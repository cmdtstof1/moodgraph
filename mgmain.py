#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
#~ cupw("#################################here")

import sys
import json

import lib.worker
from lib.db1 import db1handler
import lib.www
import lib.tgbot
import lib.mood
from lib.plot import plothandler


class App:
    '''
mood-graph main

app = application object. everything hangs on it

immer auf app.* zugreifen, ausser lokal wird explicit ein self.* = app.* gemacht
app.* darf nur von main geändert werden!!!!

run:
- source ptb/bin/activate
- python3 mgmain.py --dev=1 --test=1
- python3 mgmain.py --db1create=1
- python3 mgmain.py --tgbotrun=1
    '''

    def __init__(self, larg):
        #~ cupw(larg)
        self.cfg = self.loadcfg("cfg/cfg.json") #load default config
        self.opt = self.setDefaultOpt() #set default opt
        self.parseArg(larg) #<list >dict
        self.loadsecurecfg() #load secure cfg
        self.dtxt = self.loadtxt()
        
        self.worker = lib.worker.Worker(self)
        self.db1 = db1handler.Dbhandler(self, {})
        self.www = lib.www.Www(self, {})
        self.tgbot = lib.tgbot.Tgbot(self, {})
        self.mood = lib.mood.Mood(self, {})
        self.plot = lib.plot.plothandler.Plothandler(self, {})
        

    def setDefaultOpt(self):
        opt = {}
        opt['--test'] = 0 # runs tests TODO
        opt['--dev'] = 0 # 1= dev bot
        opt['--testptb'] = 0 # 1= tests ptb (test/ptb-test.py)
        opt['--db1create'] = 0 # 1= create db1
        opt['--db1open'] = 0 # user should opens!!!
        opt['--db1setup'] = 0 # user should setup!!!
        opt['--tgbotrun'] = 0 # runs tgb
        return opt

    def parseArg(self, larg): #<list >opt-dict, word-list
        larg = larg[1:] #remove 1. arg (=caller name)
        for a in larg:
            if a.startswith("--"):
                l1 = a.split("=")
                self.opt[l1[0]] = l1[1]
                
            else:
                cupw("TODO: what to do with opt=", a)
    
    
    def loadcfg(self, filename):
        cfg = {}
        with open(filename) as json_file:
            cfg = json.load(json_file)

        return cfg
        
    def loadsecurecfg(self):
        if self.opt['--dev']:
            with open(self.cfg['cfg']['securecfgdev']) as json_file:
                self.cfg.update(json.load(json_file))
        else:
            with open(self.cfg['cfg']['securecfgprod']) as json_file:
                self.cfg.update(json.load(json_file))

        
    
    def loadtxt(self):
        with open(self.cfg['txt']['filename']) as json_file:
            return json.load(json_file)
        
        

#######################################3
if __name__ == '__main__':
    larg = sys.argv #default for cli arguments
    
    # ~ larg = ['app', '--dev=1', '--test=1']
    # ~ larg = ['app', '--dev=1', '--test=1', '--db1create=1'] #
    # ~ larg = ['app', '--db1create=1'] #create db
    # ~ larg = ['app ', '--dev=1', '--tgbotrun=1'] #dev run
    # ~ larg = ['app ', '--tgbotrun=1'] #default run
    
    cupw("larg=", larg)
    app = App(larg)
    
    cupw("app.opt=", app.opt)
    cupw("app.cfg=", app.cfg)
    
    app.worker.main() #macht das ganze zeugs
    
    
    
