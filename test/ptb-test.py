#!/usr/bin/env python3
from lib.util.cmdtutil import prnwo as cupw
cupw("here")
# https://github.com/python-telegram-bot/python-telegram-bot/wiki/Extensions-%E2%80%93-Your-first-Bot

'''
simple standalone tester for dev bot

/home/stof/03_projekte/mood-graph/py/v2
- source ptb/bin/activate
- python3 test/ptb-test.py

'''

#load config
cfg = {}
import json
filename = "../../cfg-secure-dev.json"
with open(filename) as json_file:
    cfg.update(json.load(json_file))
    
cupw(cfg)


from telegram.ext import Updater, CommandHandler 
from telegram.ext import MessageHandler, Filters

updater = Updater(cfg['telegram']['token'], use_context=True)
dispatcher = updater.dispatcher


def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")

def hello(update, context):
    update.message.reply_text(
        'Hello {}'.format(update.message.from_user.first_name))

def echo(update, context):
    cupw("###########echo")
    context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)

def caps(update, context):
    text_caps = ' '.join(context.args).upper()
    context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps)


start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

echo_handler = MessageHandler(Filters.text & (~Filters.command), echo)
dispatcher.add_handler(echo_handler)

caps_handler = CommandHandler('caps', caps)
dispatcher.add_handler(caps_handler)

updater.dispatcher.add_handler(CommandHandler('hello', hello))

updater.start_polling()
updater.idle()
